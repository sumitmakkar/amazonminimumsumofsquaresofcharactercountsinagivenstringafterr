#include <iostream>
#include <vector>

using namespace std;

typedef struct node_structure
{
    int                   data;
    struct node_structure *next;
}node_struct;

class Queue
{
    private:
        node_struct *front;
        
        node_struct* createNode(int val)
        {
            node_struct *newNode = new node_struct();
            if(newNode)
            {
                newNode->data = val;
                newNode->next = NULL;
            }
            return newNode;
        }
        
    public:
        Queue()
        {
            front = NULL;
        }
        
        void push(int val)
        {
            node_struct *newNode = createNode(val);
            if(!front)
            {
                front = newNode;
            }
            else
            {
                node_struct *mHead = front;
                if(mHead->data < newNode->data)
                {
                    newNode->next = front;
                    front = newNode;
                      }
                else
                {
                    while(mHead->next)
                    {
                        if(mHead->next->data < newNode->data)
                        {
                            break;
                        }
                        mHead = mHead->next;
                    }
                    if(mHead->next)
                    {
                        newNode->next = mHead->next;
                        mHead->next = newNode;
                    }
                    else
                    {
                        mHead->next = newNode;
                    }
                }
            }
        }
        
        int pop()
        {
            if(front == NULL)
            {
                return 0;
            }
            front = front->next;
            return 1;
        }
};

class Engine
{
    private:
        string      inputString;
        vector<int> alphaVector;
        int         kRemovals;
        Queue       q;
        
        vector<int> initializeAlphaVector(vector<int> vec , int size)
        {
            for(int i = 0 ; i < size ; i++)
            {
                vec.push_back(0);
            }
            return vec;
        }
        
        void populateAlphaVector()
        {
            int len = (int)inputString.length();
            for(int i = 0 ; i < len ; i++)
            {
                alphaVector[inputString[i]-'a']++;
            }
        }
        
    public:
        Engine(string str , int k)
        {
            inputString = str;
            kRemovals   = k;
            alphaVector = initializeAlphaVector(alphaVector , 26);
            q           = Queue();
        }
        
        int findMinimumSumOfSquares()
        {
            populateAlphaVector();
            for(int i = 0 ; i < 26 ; i++)
            {
                q.push(alphaVector[i]);
            }
            
        }
};

int main(int argc, const char * argv[])
{
    
    return 0;
}
